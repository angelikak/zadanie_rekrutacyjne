## Aplikacja do zarządzania rezerwacją miejsc.

### Zadanie

Aplikacja pokrywa częściową implementację systemu do zarządzania rezerwacją miejsc. Pozwala na ręczny wybór miejsc do rezerwacji spośród dostępnych, które są pobierane z api. Zapisuje w globalnym stanie ilość miejsc, jaką użytkownik chciałby zareserwować, informację czy dane miejsca mają być koło siebie oraz stan rezerwacji. Posiada algorytm umożliwiający automatyczne generowanie takich miejsc na podstawie przesyłanych danych, jednak nie został on jeszcze wykorzystany w aplikacji.

### Technologie
Wykorzystałam React, Redux oraz ant.design.

### Wygląd poszczególnych stron

![results/startPage.png](results/startPage.png)

![results/seatsPage.png](results/seatsPage.png)

![results/summaryPage.png](results/summaryPage.png)
