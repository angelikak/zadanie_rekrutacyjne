import SeatsTable from "../../components/SeatsTable/SeatsTable";
import {Link} from 'react-router-dom';
import {Button} from 'antd';
import Seat from "../../components/SeatsTable/Seat/Seat";
import './SeatsPage.css';

const SeatsPage = () => {
    
    return (
        <div className="SeatsPage">
            <SeatsTable />

            <div className="SeatsPage__container">
                <div className="SeatsPage__item">
                    <Seat type="reserved" />
                    <span>Miejsca zarezerwowane</span>
                </div>
                <div className="SeatsPage__item">
                    <Seat type="avaliable" />
                    <span>Miejsca dostępne</span>
                </div>
                <div className="SeatsPage__item">
                    <Seat type="chosen" />
                    <span>Twój wybór</span>
                </div>

                <Link to='/summary'>
                    <Button type="primary">Zatwierdź</Button>
                </Link>
            </div>
        </div>
    )
}

export default SeatsPage;
