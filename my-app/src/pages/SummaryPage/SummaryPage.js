import './SummaryPage.css';
import { connect } from "react-redux";

const SummaryPage = ({reservedData}) => {

    return (
        <div className="SummaryPage">
            <h1>Twoja rezerwacja przebiegła pomyślnie</h1>
            <h3>Wybrałeś miejsca:</h3>
            <ul>
                {
                    reservedData && reservedData.map((seat, ix) => (
                        <li key={ix}>miejse: {seat.id} o współrzędnych {seat.cords.x} x oraz {seat.cords.y} y</li>
                    ))
                }
            </ul>

            <div>
                Dziękujemy! W razie problemów prosimy o kontakt z działem administracji.
            </div>
        </div>
    )
}

const mapStateToProps = state => {
    return {
        reservedData: state.reservationReducer
    }
}

export default connect(mapStateToProps)(SummaryPage);