import { InputNumber, Button, Checkbox } from 'antd';
import './StartPage.css';
import React, { useEffect, useState } from 'react';
import {Link} from 'react-router-dom';
import {useDispatch, connect} from 'react-redux';
import { checkNext, setAmount } from '../../actions';
import * as actionCreators from '../../reducers'; 

const StartPage = () => {
    const [value, setValue] = useState(0);
    const dispatch = useDispatch();

    useEffect(() => {
        const element = document.getElementById("amount");
        dispatch(setAmount(element.value));
    },[])

    const handleChange = () => {
        const element = document.getElementById("amount");
        setValue(element.value);   
        dispatch(setAmount(element.value));
    }

    /*
    useEffect(() => {
        if(element.value < 2) {
            document.getElementById("next").disabled = true;
        }
    },[]);

    useEffect(() => {
        if(value >= 2) {
            document.getElementById("next").disabled = false;
        }
    },[value]);
    */



    return (
        <div className="StartPage">
            <div className="StartPage__content">
                <label>Liczba miejsc:
                    <InputNumber 
                        id="amount"
                        name="amount"
                        min={0} 
                        max={115} 
                        defaultValue={1}
                        onClick={handleChange}
                        className="StartPage__input"
                    />
                </label>
                <div>
                    <Checkbox
                        id="next" 
                        onClick={() => {
                            dispatch(checkNext())
                        }}>Czy miejsca mają być obok siebie?</Checkbox>
                </div>
                <div>
                    <Link to='/seats'>
                        <Button type="primary">Dalej</Button>
                    </Link>
                </div>            
            </div>
        </div>
    )
}

const mapStateToProps = (state) => {
    return state;
}

export default connect (mapStateToProps, actionCreators)(StartPage);
