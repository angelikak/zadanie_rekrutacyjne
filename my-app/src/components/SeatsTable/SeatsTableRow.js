import React from 'react';
import SeatsTableCell from './SeatsTableCell';

const SeatsTableRow = ({rowId}) => {
    return (
        <tr>
            {
                Array.from(Array(15).keys()).map((ix) => (
                    <SeatsTableCell colId={--ix} rowId={rowId} />
                ))
            }
        </tr>
    );
};

export default SeatsTableRow;