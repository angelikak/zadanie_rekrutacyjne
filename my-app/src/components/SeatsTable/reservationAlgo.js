const reservationAlgo = (seatsData, isNext, reservedAmount, markSeat) => {
    let tempChosen = [];
    let currChosen = 0;

    if(seatsData !== undefined && Object.keys(seatsData).length !== 0 &&
       seatsData.seats !== undefined && seatsData.seats.length > 0) {
        for(let i=0; i<Number.parseInt(seatsData.seats.length); i++) {
            if(!isNext) {
                for(let j=0; j<Number.parseInt(reservedAmount); j++) {
                    if(!(seatsData.seats[i].reserved)) {
                        markSeat(seatsData.seat[i]);
                    }
                }
            } else {
                if(currChosen === reservedAmount) {
                    break;
                }
                if(!(seatsData.seats[i].reserved)) {
                    currChosen++;
                    tempChosen.push(seatsData.seats[i]);
                } else {
                    currChosen = 0;
                    tempChosen = [];
                }
            }
        }
    }

    for(let i=0; i<tempChosen.length; i++) {
        markSeat(tempChosen[i]);
    }

}

export default reservationAlgo;