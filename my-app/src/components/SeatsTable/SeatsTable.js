import { useEffect } from "react";
import SeatsTableRow from './SeatsTableRow';
import './SeatsTable.css';
import { connect } from "react-redux";
import { fetchSeats, markSeat } from "../../actions";
import reservationAlgo from "./reservationAlgo";

const SeatsTable = ({fetchSeats, seatsData, reservedAmount, isNext, markSeat}) => {
    useEffect(() => {
        fetchSeats();
    }, [])
    
    /*
    console.log(reservedAmount);
    console.log(seatsData.seats.length);
    console.log(seatsData);
    
    useEffect(() => {
        reservationAlgo(seatsData, isNext, reservedAmount, markSeat);
    },[seatsData])
    */
    
    return (
        <div className="SeatsTable">
            {seatsData.loading ? (
                <h2>loading...</h2>
            ) : (
                seatsData.error ? (
                    <h2 style={{color: 'red'}}>{seatsData.error}</h2>
                ) : (
                    <table className="Table">
                        <thead>
                            <tr>
                                {Array.from(Array(15).keys()).map((id) => (
                                    id === 0 ? (
                                        <th className="Table__header-first" />
                                    ) : (
                                        <th className="Table__header">{--id}</th>
                                    )
                                ))}
                            </tr>
                        </thead>
                        <tbody>
                        {
                            Array.from(Array(10).keys()).map((ix) => (
                                <SeatsTableRow rowId={ix} />
                            ))
                        }
                        </tbody>
                    </table>
                )
            )}
        </div>
    )
}
            
const mapStateToProps = state => {
    return {
        seatsData: state.seatsReducer,
        reservedData: state.reservationReducer,
        reservedAmount: state.amountReducer.value,
        isNext: state.nextReducer
    }
}

const mapDispatchToProps = (dispatch, seat) => {
    return {
        fetchSeats: () => dispatch(fetchSeats()),
        markSeat: (seat) => dispatch(markSeat(seat))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SeatsTable);