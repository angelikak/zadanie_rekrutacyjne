import React, {useEffect, useState} from 'react';
import Seat from './Seat/Seat';
import { connect, useDispatch } from "react-redux";
import { markSeat, cancelSeat } from '../../actions';

const SeatsTableCell = ({rowId, colId, seatsData, maxValue, reserved}) => {
    const [chosen, setChosen] = useState(false);
    const seat = seatsData && seatsData.filter(obj => obj.cords.x === rowId && obj.cords.y === colId)[0];
    const dispatch = useDispatch();
    let type = seat && (seat.reserved ? 'reserved' : (
        chosen ? 'chosen-modifiable' : 'avaliable-modifiable' )
    ) || 'avaliable-modifiable'

    /*
    console.log(maxValue);
    useEffect(() => {
        reserved && console.log(reserved.seats);
    },[reserved])
    */

    const handleClick = () => {
        
        if(!chosen) {
            setChosen(true);
            dispatch(markSeat(seat));
        } else {
            setChosen(false);
            dispatch(cancelSeat(seat));
        }
    }

    const resetClickHandler = () => {
      return;
    }


    return (
        <td className="SeatsTableCell" onClick={seat && !seat.reserved && handleClick || resetClickHandler}>
            <div className="TableCell__content">
                   {
                        ((colId === -1 && `${rowId}`) || (seat && 
                            <Seat 
                                seat={seat} 
                                type={type}
                            />))
                   }
            </div>
        </td>
    );
}


const mapStateToProps = state => {
    return {
        seatsData: state.seatsReducer.seats,
        maxValue: state.amountReducer.value,
        //reserved: state.reservationReducer
    }
}

export default connect(mapStateToProps)(SeatsTableCell);
