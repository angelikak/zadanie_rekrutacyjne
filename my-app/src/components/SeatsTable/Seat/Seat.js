import React from 'react';
import './Seat.css'

const Seat = ({seat, type = ''}) => {
    const classes = `Seat Seat-${type}`;

    return (
    <div className={classes}>
        {seat && seat.id}
    </div>
    )
}

export default Seat;