import React from 'react';
import './App.css'
import SeatsPage from './pages/SeatsPage/SeatsPage';
import StartPage from './pages/StartPage/StartPage';
import SummaryPage from './pages/SummaryPage/SummaryPage';
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";

function App() {
  return (
    <Router>
      <Switch>
        <Route path='/seats'>
          <SeatsPage />
        </Route>
        <Route path='/summary'>
          <SummaryPage />
        </Route>
        <Route path='/'>
          <StartPage />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
