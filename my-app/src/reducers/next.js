const nextReducer = (state = false, action) => {
    switch(action.type) {
        case 'CHECK_NEXT':
            return !state;
        default:
            return state;    
    }
}

export default nextReducer;