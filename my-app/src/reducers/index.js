import nextReducer from './next';
import amountReducer from './amount';
import {combineReducers} from 'redux';
import seatsReducer from './seats';
import reservationReducer from './reservation';

const allReducers = combineReducers ({
    nextReducer,
    amountReducer,
    seatsReducer,
    reservationReducer
})

export default allReducers;