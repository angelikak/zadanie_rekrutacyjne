const amountReducer = (state = 0, action) => {
    switch(action.type) {
        case 'SET_AMOUNT':
            return action.payload;
        default:
            return state;
    }
}

export default amountReducer;