const reservationReducer = (state = [], action) => {
    switch(action.type) {
        case 'MARK_SEAT':
            return [...state, action.payload];
        case 'CANCEL_SEAT':
            return state.filter(id => id !== action.payload);
        default:
            return state;    
    }
}

export default reservationReducer;