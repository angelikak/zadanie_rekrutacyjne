import axios from 'axios';

export const setAmount = (input) => {
    return {
        type: 'SET_AMOUNT',
        payload: {
            value: input
        }
    }
}

export const checkNext = () => {
    return {
        type: 'CHECK_NEXT'
    }
}

const fetchSeatsRequest = () => {
    return {
        type: 'FETCH_SEATS_REQUEST'
    }
}

const fetchSeatsSuccess = (seats) => {
    return {
        type: 'FETCH_SEATS_SUCCESS',
        payload: seats
    }
}

const fetchSeatsFailure = (error) => {
    return {
        type: 'FETCH_SEATS_FAILURE',
        payload: error
    }
}

export const fetchSeats = () => {
    return async (dispatch) => {
        dispatch(fetchSeatsRequest);
        try {
            const response = await axios.get('http://localhost:3000/seats');
            const seats = response.data;
            dispatch(fetchSeatsSuccess(seats));
        } catch (error) {
            const errorMsg = error.message;
            dispatch(fetchSeatsFailure(errorMsg));
        }
    }
}

export const markSeat = (item) => {
    return {
        type: 'MARK_SEAT',
        payload: item
    }
}

export const cancelSeat = (id) => {
    return {
        type: 'CANCEL_SEAT',
        payload: id
    }
}